#include <Mqttdatasend.h>

WiFiClient client;
PubSubClient mqttClient (client);

Mqttdatasend::Mqttdatasend(){ //LLAMO AL CONSTRUCTOR PARA QUE NO ME DE ERROR

}

void Mqttdatasend::mqqtBegin()
{
  mqttClient.setServer(broker, 1883);   // Set the MQTT broker details
  mqttClient.setCallback([this](char *topic, byte *payload, unsigned int messageLength) { this->mqttSubscriptionCallback(topic, payload, messageLength); });;   // Set the MQTT message handler function.
  timer.countdown_ms(18000);
}

void Mqttdatasend::mqttSubscriptionCallback(char* topic, byte* payload, unsigned int messageLength)
{
  Serial.println("Message arrived [" +String(topic)+ "] ");
  Serial.println((char)payload[0]);
  Serial.println((char)payload[1]);
}

void Mqttdatasend::mqttLoop()
{
  if (subscribeflag== false)
  {
    mqttdatasend.mqttSubscribe();
    subscribeflag=true;
  }
  if (timer.expired())
  {
    timer.countdown_ms(18000);
    mqttdatasend.mqttPublish();
  }
  mqttClient.loop();  // Call the loop continuously to establish connection to the server.
}

void Mqttdatasend::reconnect()
{
  while (!mqttClient.connected())
  {
    Serial.print("tratando conectar con el mqtt broker..");
    // Connect to the MQTT broker.
    if (mqttClient.connect(mqttUserName))
    {
      Serial.println("connected");
      mqttClient.publish("casa/oficina/randomnum", "hello from mqtt");
      mqttflag=true;
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 4 seconds");
      delay(4000);
    }
  }
  // mqttdatasend.mqqtGetStatus();
}

int Mqttdatasend::mqttSubscribe()
{
  String myTopic;
  myTopic="casa/oficina/randomnum";
  Serial.println( "Subscribing to " + myTopic );
  Serial.println( "State= " + String( mqttClient.state() ) );
  return mqttClient.subscribe(myTopic.c_str() ,0);
}

void Mqttdatasend::mqttPublish()
{
  dataToPublish = random(1,100);
  Serial.println("numero random:"+String(dataToPublish));
  // Create data string to send to ThingSpeak.
  String data = String("field1=" + String(dataToPublish));
  int length = data.length();
  char msgBuffer[length];
  data.toCharArray(msgBuffer,length+1);
  //imprimo el string que enviare a TS
  Serial.println(msgBuffer);
  mqttClient.publish( "casa/oficina/randomnum", msgBuffer);
}

boolean Mqttdatasend::mqqtGetStatus()
{
  return mqttflag;
}


Mqttdatasend mqttdatasend;
