#ifndef EEPROMCONTROL_H
#define EEPROMCONTROL_H
#include <Arduino.h>
#include <EEPROM.h>
//defino la constante del limite de mis arreglos
#define LIMIT_INPUT 50

class Eepromcontrol
{
private:
  //variables privadas
public:
  //variables publicas
  char ssidread[50];
  char passread[50];
  //constructor
  Eepromcontrol();
  //metodos
  void eepromBegin();
  void saveSSID(String a);
  void savePassword(String b);
  String read(int addr);

};
extern Eepromcontrol eepromcontrol;
#endif
