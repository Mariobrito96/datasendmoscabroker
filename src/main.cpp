#include <Arduino.h>
#include <Conectionmode.h>
#include <Buttoncontrol.h>
#include <Eepromcontrol.h>
#include <Mqttdatasend.h>


void setup()
{
  //inicio puerto serial
  conectionmode.conectionModeBegin();
  //llamo a la funcion para iniciar la eeprom
  eepromcontrol.eepromBegin();
  //inicializo los pines de leds y boton
  buttoncontrol.buttonBegin();
  //realizo el setserver y el setcallback
  mqttdatasend.mqqtBegin();
}
// loop
void loop()
{
  buttoncontrol.buttonLoop();
  //condición boton
  if (buttoncontrol.getState() == true)
  {
    Serial.println("conección AP");
    conectionmode.coneccionAp();
  }
  else
  {
    //si no esta conectado a la red
    if (conectionmode.wifiGetStatus()==false)//condicion para conectarse al modem (si ya se conecto no entra)
    {
      conectionmode.wifiBegin();//coneccion wifi
    }
    //si esta conectado a la red
    if (conectionmode.wifiGetStatus() == true)
    {
      //si esta conectado el broker de mosca
      if (mqttdatasend.mqqtGetStatus() == true)
      {
        //entra al loop de la clase mqttdatasend
        mqttdatasend.mqttLoop();
      }
      //si no esta conectado el broker de mosca
      else
      {
        //conectate al boker de mosca
        mqttdatasend.reconnect();
      }
    }
  }
}
