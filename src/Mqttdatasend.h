#ifndef MQTTDATASEND_H
#define MQTTDATASEND_H
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <VirtualTimer.h>

class Mqttdatasend
{
private:
  // MQTT broker settings
  const char* broker ="192.168.0.10";
  const char* mqttUserName = "ESP-01-CLIENT1234";       // Use any name.

public:
  //variables publicas
  VirtualTimer timer;
  long dataToPublish;   // Let the main loop know ther is new data to set.
  //banderas
  boolean mqttflag=false;
  boolean subscribeflag=false;
  //constructor
  Mqttdatasend();
  //metodos
  void mqqtBegin();
  void mqttSubscriptionCallback(char* topic, byte* payload, unsigned int messageLength);
  void mqttLoop();
  void reconnect();
  int mqttSubscribe();
  void mqttPublish();
  boolean mqqtGetStatus();
};
extern Mqttdatasend mqttdatasend;
#endif
