//* esta clase controla  los tipos de conecccion que se llevaran
// a cabo en este programa, asi mismo se encviaran los datos
// y se guardaran los datos en la EEPROM
#ifndef CONECTIONMODE_H
#define CONECTIONMODE_H
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Eepromcontrol.h>

class Conectionmode
{
private:
//variables privadas
  String ssidAP = "RedBrito";
  boolean wififlag=false;
  char ssidread[50];
  char passread[50];
  String ssid;
  String password;
public:
  //variables publicas
Conectionmode();//constructor
//metodos
void conectionModeBegin();
void conectionModeLoop();
void wifiBegin();
void coneccionAp();
void serverOn();
void sendData();
boolean wifiGetStatus();
String ssidValue();
String passValue();

};
extern Conectionmode conectionmode;
#endif
