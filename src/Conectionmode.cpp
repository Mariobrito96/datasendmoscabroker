#include <Conectionmode.h>

ESP8266WebServer server(80);

Conectionmode::Conectionmode(){ //LLAMO AL CONSTRUCTOR PARA QUE NO ME DE ERROR

}
//este metodo solo inicializa el puerto serial
void Conectionmode::conectionModeBegin()
{
  Serial.begin(115200);
  Serial.println();
}

void Conectionmode::wifiBegin()
{
  //solicito los datos para conectarme extrayengtrayendolos de la EEPROM
  eepromcontrol.read(0).toCharArray(ssidread, 50);
  eepromcontrol.read(50).toCharArray(passread, 50);
  //convierto de char a string
  ssid = String(ssidread);
  password= String(passread);
  delay(3000);
  Serial.print(".");
  delay(500);
  WiFi.mode(WIFI_STA);       //para que no inicie el SoftAP en el modo normal
  WiFi.begin(ssid, password);//mystringvariable.c_str() convierte un string a char

  if (WiFi.status() == WL_CONNECTED) // si hay conecccion wifi
  {
    Serial.println("");
    Serial.println("WiFi connected");
    WiFi.printDiag(Serial);// te imprime todas las caracteristicas de la coneccion wifi
    Serial.println(WiFi.localIP());
    wififlag=true;//cambiamos el estado de la bandera para que no se cicle en el loop del main
  }
}

boolean Conectionmode::wifiGetStatus()
{
  // retorno el valor de wififlag para evaluar el valor retornado en el loop del main
  return wififlag;
}

void Conectionmode::coneccionAp()
{
  conectionmode.serverOn();
  digitalWrite(2, HIGH);
  Serial.print("Setting soft-AP ... ");
  // me conecto como punto de aceso
  boolean result = WiFi.softAP(ssidAP);//mando nombre de la red creada, se le puede añadir la contraseña
  if(result)
  {
    Serial.println("Ready");
  }
  else
  {
    Serial.println("Failed!");
  }
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("IP:");
  Serial.println(myIP);
  while(1){
    conectionmode.conectionModeLoop();
  }
}

void Conectionmode::serverOn()
{
  // en caso de recivir la URL "/config" ejecuto el metodo Conectiontype::sendData
  // inicializo el server
  server.on("/config", std::bind(&Conectionmode::sendData, this));
  server.begin();
  Serial.println("HTTP server started");
}

void Conectionmode::sendData()
{
  eepromcontrol.saveSSID(conectionmode.ssidValue());
  eepromcontrol.savePassword(conectionmode.passValue());
  eepromcontrol.read(0).toCharArray(ssidread, 50);
  eepromcontrol.read(50).toCharArray(passread, 50);
  Serial.println(ssidread);
  Serial.println(passread);
  delay (500);
  ESP.restart();
  //reset
}

void Conectionmode::conectionModeLoop()
{
  server.handleClient();
}

String Conectionmode::ssidValue()
{
  String ssid = server.arg("ssid");
  return ssid;
}

String Conectionmode::passValue()
{
  String pass= server.arg("pass");
  return pass;
}

Conectionmode conectionmode;
